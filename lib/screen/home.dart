import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../services/sakai_services.dart';

class HomeScreen extends StatefulWidget {

  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return HomeScreenState();
  }
}

class HomeScreenState  extends State<StatefulWidget> {
  SakaiService sakaiService = SakaiService(sakaiUrl: "https://xlms.myworkspace.vn");
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Courses')
      ),
      body: SafeArea(
        child: FutureBuilder<List<String>>(
          future: getCurrentCourses(),
          builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
            if(snapshot.connectionState == ConnectionState.done){
              return ListView.builder(
                itemCount: snapshot.data.length,
                itemBuilder: (context,index){
                  return Card(
                    elevation: 5.0,
                    shape: const RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(10.0)),
                    ),
                    color: Colors.white70,
                    margin: const EdgeInsets.all(20),
                    child: Text('${snapshot.data[index]}'),
                  );
                },
              );
            }
            else if(snapshot.connectionState == ConnectionState.waiting){
              return Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: const <Widget>[
                    SizedBox(
                      child: CircularProgressIndicator(),
                      width: 60,
                      height: 60,
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: 16),
                      child: Text('Loading data from API Sakai services...'),
                    )
                  ],
                ),
              );
            }
            else{
              return Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: const <Widget>[
                    Padding(
                      padding: EdgeInsets.only(top: 16),
                      child: Text("Something went wrong we can't load data from API Sakai services..."),
                    )
                  ],
                ),
              );
            }
          },
        ),
      ),
    );
  }


  Future<List<String>> getCurrentCourses() async{
    List<String> coursesList = [];
    http.Response result = await sakaiService.getSites();
    print("result: ");
    var jsonSites = json.decode(result.body);
    print('parse to JSON; result:=\n${result.body}');
    print(jsonSites['site_collection'].toString());

    for(int i=0;i<jsonSites['site_collection'].toList().length;i++){
      print("Entity:");
      print(jsonSites['site_collection'][i]['entityTitle']);
      coursesList.add(jsonSites['site_collection'][i]['entityTitle']);
    }
    return coursesList;
  }

}


















































