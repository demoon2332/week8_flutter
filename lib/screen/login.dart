
import 'dart:convert';

import 'package:app_sakai_services/services/sakai_services.dart';
import 'package:flutter/material.dart';
import '../validator/login_validator.dart';
import 'home.dart';
import 'package:http/http.dart' as http;

class LoginScreen extends StatefulWidget {

  const LoginScreen({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return LoginScreenState();
  }
}

class LoginScreenState extends State<StatefulWidget> with LoginValidator {
  SakaiService sakaiService = SakaiService(sakaiUrl: "https://xlms.myworkspace.vn");
  final formKey = GlobalKey<FormState>();
  final emailController = TextEditingController();
  final passController = TextEditingController();
  String message = '';

  @override
  void dispose() {
    emailController.dispose();
    passController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
        appBar: AppBar(
          title: const Center(
            child: Text('Sakai Services App Login'),
          ),
        ),
        body: SafeArea(
          child: Padding(
            padding: const EdgeInsets.all(32),
            child: Form(
              key: formKey,
              child: Column(
                children: [
                  Row(
                    children: [
                      const SizedBox(
                        height: 20,
                      ),
                      Expanded(child: emailField())
                    ],
                  ),
                  Row(
                    children: [
                      const SizedBox(height: 20),
                      Expanded(
                        child: passwordField(),
                      )
                    ],
                  ),
                  Row(
                    children: [
                      const SizedBox(height: 20),
                        loginButton(context),
                    ],
                  ),
                  Row(
                    children: [
                      const SizedBox(height: 20),
                        Text(message,style: const TextStyle(color: Colors.redAccent),),
                    ],
                  ),
                ],
              ),
            ),
          ),
        ) //body
    );
  }

  Widget passwordField() {
    return TextFormField(
      controller: passController,
      obscureText: true,
      decoration: const InputDecoration(
          border: UnderlineInputBorder(),
          icon: Icon(Icons.password),
          labelText: 'Password:',
          hintText: 'Enter password'),
      autofocus: false,
      textCapitalization: TextCapitalization.none,
      keyboardType: TextInputType.visiblePassword,
      autocorrect: false,
      validator: validatePass,
    );
  }

  Widget emailField() {
    return TextFormField(
      controller: emailController,
      decoration: const InputDecoration(
        icon: Icon(Icons.person),
        border: UnderlineInputBorder(),
        labelText: 'Email/Username:',
        hintText: 'Enter email or username',
      ),
      autocorrect: false,
      autofocus: false,
      textCapitalization: TextCapitalization.none,
      keyboardType: TextInputType.emailAddress,
      validator: validateEmail,
    );
  }

  Widget loginButton(BuildContext context) {
    return ElevatedButton(
        onPressed: (){
          print("EmaiL: "+emailController.text);
          print("Pass: "+passController.text);
          login();
        },
        child: const Padding(
          padding: EdgeInsets.all(6),
          child: Text('Login'),
        ));
  }

    void login() async {
    http.Response response = await sakaiService.authenticate(emailController.text,passController.text);

    print(response.toString());

    if(response.statusCode == 200 || response.statusCode == 201){
      Navigator.of(context)
          .push(MaterialPageRoute(builder: (_) => const HomeScreen()));
      return null;
    }
    else{
      setState(() {
        message = "Wrong username or password.";
      });
    }
  }


}
