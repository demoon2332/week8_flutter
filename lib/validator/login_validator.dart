mixin LoginValidator {
  String? validateEmail(String? email) {
    if (email!.isEmpty) {
      //return 'Email address is required.';
      return null;
    }
    final regex = RegExp('[^@]+@[^\.]+\..+');
    if (!regex.hasMatch(email)) {
      //return 'Enter your email';
      return null;
    }
    else
      return null;
  }


  String? validatePass(String? pass){
    String  pattern = r'^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[!@#\$&*~]).{8,}$';
    RegExp regExp = new RegExp(pattern);
    if(regExp.hasMatch(pass!))
      return null;
    else
      return null;
    //   return "Password must be longer than 8 and include"
    //   +"\n At least 1 uppercase character"
    // +"\n At least 1 lowercase character"
    // +"\n At least 1 special character.";
  }
}